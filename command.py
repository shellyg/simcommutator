class Command:
    def __init__(self, function, nargs, args):
        self.function = function
        self.nargs = nargs
        self.args = args

    def get_command_as_dict(self):
        return self.__dict__

    def __getitem__(self, item):
        return self.get_command_as_dict()[item]
