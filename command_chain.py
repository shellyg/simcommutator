from command import *


class CommandChain:
    def __init__(self, commands):
        self.commands = []

        for command in commands:
            self.add_command(command)

    def add_command(self, command):
        if isinstance(command, Command):
            self.commands.append([command])
        elif isinstance(command, list) and command and isinstance(command[0], Command):
            self.commands.append(command)
        else:
            raise TypeError("command must be of type Command or list of commands")

    def get_commands(self):
        return self.commands

    def __len__(self):
        return self.commands.__len__()

    def __getitem__(self, item):
        return self.commands[item]
