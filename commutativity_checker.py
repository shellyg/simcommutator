import itertools
import time
import imp
import getopt
import sys
from z3 import *

from print_colors import *

unsat_prefixes = []
g_verbose = False


def find_violations_or_prove_for_subset(S, op_a, op_b, start_len, remaining_len, chain):
    global g_verbose
    if g_verbose:
        print PrintColors.HEADER + "depth: %d" % (start_len - remaining_len)
        print PrintColors.HEADER + "start_len = %d, remaining_len = %d" % (start_len, remaining_len)
        print chain

    if chain.startswith(tuple(unsat_prefixes)):
        print PrintColors.RED + "unsat because matches an unsat prefix"
        return

    if remaining_len == 0:
        result = S.check()

        if g_verbose:
            print PrintColors.RED,
            print result

        if result == sat:
            if g_verbose:
                print S.model()
        elif result == unsat:
            if not g_verbose: # because if verbose, this info is already printed
                print chain
                print PrintColors.RED,
                print result
                print ""

            unsat_prefixes.append(chain)
            if g_verbose:
                print S.unsat_core()

        if g_verbose:
            print ""
        return

    # option 1 - from a
    # Backup S - create new context!
    # S_a = copy.deepcopy(S)  - no need, use push/pop! :)
    if op_a:
        S.push()
        for command in op_a[0]:
            S.add(command['function'](start_len - remaining_len, *(command['args'])))

        new_chain = chain + PrintColors.BLUE + op_a[0][0]['function'].name() + '(' + ','.join(
            map(str, op_a[0][0]['args'])) + '); '

        find_violations_or_prove_for_subset(S, op_a[1:], op_b, start_len, remaining_len - 1, new_chain)
        S.pop()

    # option 2 - from b
    if op_b:
        S.push()
        for command in op_b[0]:
            S.add(command['function'](start_len - remaining_len, *(command['args'])))

        new_chain = chain + PrintColors.GREEN + op_b[0][0]['function'].name() + '(' + ','.join(
            map(str, op_b[0][0]['args'])) + '); '

        find_violations_or_prove_for_subset(S, op_a, op_b[1:], start_len, remaining_len - 1, new_chain)
        S.pop()


def find_violations_or_prove(S,
                             # a solver loaded with begin and end constraints as well as definitions of uninterpreted functions
                             op_a,
                             # operations of thread a. Should be a dictionary of the BoolSort functions to its consts
                             op_b  # operations of thread b. -"-
                             ):
    res = False

    # Loop on schedules: start with len 2 and increase
    for i in range(2, len(op_a) + len(op_b) + 1):
        print PrintColors.TITLE + "Checking for a chain of operations of length %d" % (i)
        chain = ""
        find_violations_or_prove_for_subset(S, op_a, op_b, i, i, chain)

        print PrintColors.HEADER + "-----------------------------------------------------> Done!"

        """ if len==0 check, otherwise recursive call on both choices """

    # If no unset prefixes are found, we can assume this scenario is indeed commutative
    if not unsat_prefixes:
        res = True

    # When done, clear the unsat prefixes list
    del unsat_prefixes[:]

    return res


def bound_vars_in_constraint_universally(constraint, list_of_variables):
    for var in list_of_variables:
        constraint = ForAll(var, constraint)

    return constraint


def fill_system_constraints(S, model, funcs):
    # Just one operation in a time unit
    # Note this is not related/dependent on the model, but it should be part of the "core" model. The user does not have to specify it manually.
    for func1, func2 in itertools.product(funcs, funcs):

        # Hard case - both functions are the same. So impossible to invoke in some time unit the same function with different arguments - take from two sets of example args.
        if func1 == func2:
            # print zip(model.example_args[func1.name()], model.example_args2[func2.name()])  # First from both lists

            pairwise_different_args = False  # Going to 'or' so start with false
            for x, y in zip(model.example_args[func1.name()], model.example_args2[func2.name()]):
                pairwise_different_args = Or(pairwise_different_args,
                                             x != y)  # At least one pair is different from each other

            # A certain function f in time unit t - impossible to have the same f invoked with at least one argument which is different
            implication = Implies(func1(model.t, *model.example_args[func1.name()]),
                                  Not(And(func2(model.t, *model.example_args2[func2.name()]), pairwise_different_args)))

            # Bounding the variables in the implication:
            constraint = bound_vars_in_constraint_universally(implication,
                                                              model.example_args2[func2.name()] + model.example_args[
                                                                  func1.name()] + [model.t])

            S.add(constraint)

        # Easy case - no 2 invocations of different functions in a time unit.
        else:
            implication = Implies(func1(model.t, *model.example_args[func1.name()]),
                                  Not(func2(model.t, *model.example_args2[func2.name()])))

            # Bounding the variables in the implication:
            constraint = bound_vars_in_constraint_universally(implication,
                                                              model.example_args2[func2.name()] + model.example_args[
                                                                  func1.name()] + [model.t])

            S.add(constraint)


def handleScenario(S, scenario, ops):
    # Scenario specific - push for scenario context
    S.push()

    print PrintColors.YELLOW + "Starting scenario '%s'" % (scenario.get_name())

    start_time = time.time()

    # Add scenario's start conditions
    scenario.add_start_conditions(S)

    # Add scenario's special conditions
    scenario.add_special_conditions(S)

    # print S

    # Do z3's work
    is_sim_commute = find_violations_or_prove(S, *ops)
    if is_sim_commute is True:
        output_message = 'SIM-commutative'
    else:
        output_message = 'not commutative'

    end_time = time.time()

    print PrintColors.YELLOW + "Time elapsed: %.3f seconds. Scenario is %s" % (end_time - start_time, output_message)
    print ""

    # Finished with the scenario - ready to pop its context
    S.pop()


# Utility method: given a module object, find all classes in it that matches certain prefix
def find_classes_in_module(module, prefix):
    module_dict = module.__dict__
    return [
        module_dict[c] for c in module_dict if (
            isinstance(module_dict[c], type) and
            module_dict[c].__module__ == module.__name__ and
            module_dict[c].__name__.startswith(prefix)
        )
        ]


# Utility method: given a module name, load and return the module object
def get_module_by_name(module_name):
    try:
        fp = None

        # Try to find the correct model
        fp, pathname, description = imp.find_module(module_name, ["."])
        ret_module = imp.load_module(module_name, fp, pathname, description)
    except ImportError as e:
        print PrintColors.RED + "ERROR: Could not import model '%s.py': %s" % (module_name, e.message)
        return None
    finally:
        # Since we may exit via an exception, close fp explicitly.
        if fp:
            fp.close()

    return ret_module


# Load model and scenario classes
def find_model_and_scenarios(name):
    model_module_to_use = "model_" + name
    scenario_module_to_use = "scenarios_" + name

    model_module = get_module_by_name(model_module_to_use)
    scenario_module = get_module_by_name(scenario_module_to_use)
    if not model_module or not scenario_module:
        print PrintColors.RED + "ERROR: Could not load model or scenario modules"
        return None, None

    all_model_class = find_classes_in_module(model_module, "Model")
    if all_model_class.__len__() != 1:
        print PrintColors.RED + "ERROR: Model module must contain exactly one model class"
        return None, None
    all_scenario_classes = find_classes_in_module(scenario_module, "Scenario")

    return all_model_class[0], all_scenario_classes


def print_usage():
    print "CommutativityChecker"
    print "\t-h - Print this help message"
    print "\t-m - Model to use (short name)"
    print "\t-s - Scenario to use. If none specified, run all scenarios"
    print "\t-v - Verbose output prints"
    print ""
    print "For example: python commutativity_checker -m fs -v"


if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hm:s:v", ["help", "model=", "scenario=", "verbose"])
    except getopt.GetoptError:
        sys.exit(2)

    model_name, scenario_to_use = [None, None]
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_usage()
            sys.exit(0)
        elif opt in ("-m", "--model"):
            model_name = arg
        elif opt in ("-s", "--scenario"):
            scenario_to_use = arg
        elif opt in ("-v", "--verbose"):
            g_verbose = True

    if model_name is None:
        print "Please select a model"
        print_usage()
        sys.exit(0)

    model_class, scenario_classes = find_model_and_scenarios(model_name)
    if not model_class or not scenario_classes:
        print "Could not load model and/or corresponding scenarios '%s'. " \
              "Please make sure the following files reside in the same directory " \
              "and are valid model and scenario files: %s.py, %s.py" % \
              (model_name, "model_" + model_name, "scenarios_" + model_name)
        sys.exit(0)

    S = Solver()

    model = model_class()
    funcs = model.get_funcs()
    model.fill_constraints(S)
    fill_system_constraints(S, model, funcs)

    for scenario_class in scenario_classes:
        if scenario_to_use and not scenario_class.__name__ == "Scenario" + scenario_to_use:
            continue

        scenario = scenario_class(model)
        handleScenario(S, scenario, [scenario.op_a, scenario.op_b])
