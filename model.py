
class Model(object):

    def __init__(self):
        pass

    def fill_constraints(self):
        raise NotImplementedError("Must implement a model sub-class")

    def get_funcs(self):
        raise NotImplementedError("Must implement a model sub-class")

    def get_name(self):
        # Default implementation: return the class name. Optionally, this can be used for more meaningful names
        return self.__class__.__name__
