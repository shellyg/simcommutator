from z3 import *


class ModelSimpleFS(Model):
    def __init__(self):
        # Declare model's sorts
        self.Time = IntSort()
        self.Directory = DeclareSort('Directory')
        self.File = DeclareSort('File')

        # Declare functions and predicates
        self.create_file = Function('create_file', self.Time, self.Directory, self.File, BoolSort())
        self.delete_file = Function('delete_file', self.Time, self.Directory, self.File, BoolSort())

        self.is_empty = Function('is_empty', self.Time, self.Directory, BoolSort())
        self.contains = Function('contains', self.Time, self.Directory, self.File, BoolSort())

        self.inc_nlinks = Function('inc_nlinks', self.Time, self.Directory, BoolSort())
        self.dec_nlinks = Function('dec_nlinks', self.Time, self.Directory, BoolSort())
        self.nop_nlinks = Function('nop_nlinks', self.Time, self.Directory, BoolSort())
        self.get_nlinks = Function('get_nlinks', self.Time, self.Directory, IntSort())

        # Assisting constants for constraint definition. Must always be bounded variables!
        self.t = Const('t', self.Time)
        self.t2 = Const('t2', self.Time)
        self.t3 = Const('t3', self.Time)

        self.dir1 = Const('dir1', self.Directory)
        self.file1 = Const('file1', self.File)
        self.dir2 = Const('dir2', self.Directory)
        self.file2 = Const('file2', self.File)

        self.dir3 = Const('dir3', self.Directory)
        self.file3 = Const('file3', self.File)
        self.dir4 = Const('dir4', self.Directory)
        self.file4 = Const('file4', self.File)

        self.arg_types = dict(
            create_file=[self.Directory, self.File],
            delete_file=[self.Directory, self.File],
            inc_nlinks=[self.Directory],
            dec_nlinks=[self.Directory]
        )

        self.example_args = dict(
            create_file=[self.dir1, self.file1],
            delete_file=[self.dir2, self.file2],
            dec_nlinks=[self.dir1],
            inc_nlinks=[self.dir2]
        )

        self.example_args2 = dict(
            create_file=[self.dir3, self.file3],
            delete_file=[self.dir4, self.file4],
            dec_nlinks=[self.dir3],
            inc_nlinks=[self.dir4]
        )

    def get_funcs(self):
        return [self.create_file, self.delete_file, self.inc_nlinks, self.dec_nlinks]

    # Fill solver with model's constraints
    def fill_constraints(self, S):
        # Cannot add before time 0
        S.add(ForAll(self.t, ForAll(self.dir1,
                                    ForAll(self.file1,
                                           Implies(self.create_file(self.t, self.dir1, self.file1), self.t >= 0)))))

        # Specify predicates

        # nlinks is the number of files in a directory

        # is empty <-> nlinks == 0
        S.add(ForAll(self.t, ForAll(self.dir1, Implies(self.is_empty(self.t, self.dir1),
                                                       self.get_nlinks(self.t, self.dir1) == Int(0)))))
        S.add(ForAll(self.t, ForAll(self.dir1, Implies(self.get_nlinks(self.t, self.dir1) == Int(0),
                                                       self.is_empty(self.t, self.dir1)))))
        # increment
        S.add(ForAll(self.t, ForAll(self.dir1, Implies(self.inc_nlinks(self.t, self.dir1),
                                                       self.get_nlinks(self.t + 1, self.dir1) == self.get_nlinks(
                                                           self.t, self.dir1) + 1))))
        # decrement
        S.add(ForAll(self.t, ForAll(self.dir1, Implies(self.dec_nlinks(self.t, self.dir1),
                                                       self.get_nlinks(self.t + 1, self.dir1) == self.get_nlinks(
                                                           self.t, self.dir1) - 1))))
        # no op
        S.add(ForAll(self.t, ForAll(self.dir1, Implies(self.nop_nlinks(self.t, self.dir1),
                                                       self.get_nlinks(self.t + 1, self.dir1) == self.get_nlinks(
                                                           self.t, self.dir1)))))

        # cannot have negative nlinks
        S.add(ForAll(self.t, ForAll(self.dir1, self.get_nlinks(self.t, self.dir1) >= Int(0))))

        # itayp: This condition for some reason, makes it too hard for Z3 to compute
        # Also tried forms such as Implies(Not(dec, inc), Value stays the same) - same result
        # As a workaround, use the "nop" predicate (means - no change)
        #S.add(ForAll(self.t, ForAll(self.dir1,
        #                            Or(self.dec_nlinks(self.t, self.dir1),
        #                               self.inc_nlinks(self.t, self.dir1),
        #                               self.get_nlinks(self.t, self.dir1) == self.get_nlinks(self.t + 1, self.dir1)))))

        # remove is_empty or define with both directions as needed
        #S.add(ForAll(self.t, ForAll(self.dir1, Implies(self.is_empty(self.t, self.dir1), ForAll(self.file1, Not(
        #    self.contains(self.t, self.dir1,
        #                  self.file1)))))))
        #S.add(ForAll(self.t, ForAll(self.dir1, ForAll(self.file1, Implies(self.contains(self.t, self.dir1, self.file1),
        #                                                                  Not(self.is_empty(self.t, self.dir1)))))))

        # Specify actions

        # specify 'create_file'
        # When adding a file to directory, it is not empty in the next point in time
        #S.add(
        #    ForAll(self.t, ForAll(self.dir1, ForAll(self.file1, Implies(self.create_file(self.t, self.dir1, self.file1),
        #                                                                Not(self.is_empty(self.t + 1,
        #                                                                                  self.dir1)))))))
        # When adding an file to directory, it will be contained in this directory
        S.add(
            ForAll(self.t, ForAll(self.dir1, ForAll(self.file1, Implies(self.create_file(self.t, self.dir1, self.file1),
                                                                        self.contains(self.t + 1, self.dir1,
                                                                                      self.file1))))))
        # specify 'delete_file'
        # When deleting a file from directory, it will not be contained in this directory in the next point in time
        S.add(
            ForAll(self.t, ForAll(self.dir1, ForAll(self.file1, Implies(self.delete_file(self.t, self.dir1, self.file1),
                                                                        Not(self.contains(self.t + 1, self.dir1,
                                                                                          self.file1)))))))
        # File is not contained in a directory after deletion
        S.add(ForAll(self.t,
                     ForAll(self.dir1, ForAll(self.file1, Implies(Not(self.contains(self.t + 1, self.dir1, self.file1)),
                                                                  Not(self.delete_file(self.t + 1, self.dir1,
                                                                                       self.file1)))))))
        # Can't delete non-existent items
        S.add(
            ForAll(self.t, ForAll(self.dir1, ForAll(self.file1, Implies(self.delete_file(self.t, self.dir1, self.file1),
                                                                        self.contains(self.t, self.dir1,
                                                                                      self.file1))))))

        # Can't 'contain' without a previous 'add' which was not 'delete'
        S.add(ForAll(self.t, ForAll(self.dir1, ForAll(self.file1, Implies(self.contains(self.t, self.dir1, self.file1),
                                                                          Or(self.t ==0,
                                                                              Exists(self.t2,
                                                                                     And(self.t2 >= 0, self.t2 < self.t,
                                                                                         self.create_file(self.t2,
                                                                                                          self.dir1,
                                                                                                          self.file1),
                                                                                         ForAll(self.t3, Implies(
                                                                                             And(self.t2 < self.t3,
                                                                                                 self.t3 < self.t),
                                                                                             Not(
                                                                                                 self.delete_file(
                                                                                                     self.t3,
                                                                                                     self.dir1,
                                                                                                     self.file1))))))))))))
