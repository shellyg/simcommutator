from z3 import *


class ModelSimpleSets(Model):

    def __init__(self):
        # Declare model's sorts
        self.Time = IntSort()
        self.Set = DeclareSort('Set')
        self.Elem = DeclareSort('Elem')

        # Declare functions and predicates
        self.add = Function('add', self.Time, self.Set, self.Elem, BoolSort())
        self.delete = Function('delete', self.Time, self.Set, self.Elem, BoolSort())

        self.is_empty = Function('is_empty', self.Time, self.Set, BoolSort())
        self.contains = Function('contains', self.Time, self.Set, self.Elem, BoolSort())

        # Assisting constants for constraint definition. Must always be bounded variables!
        self.t = Const('t', self.Time)
        self.t2 = Const('t2', self.Time)
        self.t3 = Const('t3', self.Time)

        self.set1 = Const('set1', self.Set)
        self.elem1 = Const('elem1', self.Elem)
        self.set2 = Const('set2', self.Set)
        self.elem2 = Const('elem2', self.Elem)

        self.set3 = Const('set3', self.Set)
        self.elem3 = Const('elem3', self.Elem)
        self.set4 = Const('set4', self.Set)
        self.elem4 = Const('elem4', self.Elem)

        self.arg_types = dict(
            add=[self.Set, self.Elem],
            delete=[self.Set, self.Elem]
        )

        self.example_args = dict(
            add=[self.set1, self.elem1],
            delete=[self.set2, self.elem2]
        )

        self.example_args2 = dict(
            add=[self.set3, self.elem3],
            delete=[self.set4, self.elem4]
        )

    def get_funcs(self):
        return [self.add, self.delete]

    # Fill solver with model's constraints
    def fill_constraints(self, S):
        # Cannot add before time 0
        S.add(ForAll(self.t, ForAll(self.set1,
                                    ForAll(self.elem1, Implies(self.add(self.t, self.set1, self.elem1), self.t >= 0)))))

        # Specify predicates

        # remove is_empty or define with both directions as needed
        S.add(ForAll(self.t, ForAll(self.set1, Implies(self.is_empty(self.t, self.set1), ForAll(self.elem1, Not(
            self.contains(self.t, self.set1,
                          self.elem1)))))))
        S.add(ForAll(self.t, ForAll(self.set1, ForAll(self.elem1, Implies(self.contains(self.t, self.set1, self.elem1),
                                                                          Not(self.is_empty(self.t, self.set1)))))))

        # Specify actions

        # specify 'add'
        # When adding an object to set, it is not empty in the next point in time
        S.add(ForAll(self.t, ForAll(self.set1, ForAll(self.elem1, Implies(self.add(self.t, self.set1, self.elem1),
                                                                          Not(self.is_empty(self.t + 1,
                                                                                            self.set1)))))))
        # When adding an object to set, it will be contained in this set
        S.add(ForAll(self.t, ForAll(self.set1, ForAll(self.elem1, Implies(self.add(self.t, self.set1, self.elem1),
                                                                          self.contains(self.t + 1, self.set1,
                                                                                        self.elem1))))))
        # S.add(ForAll(t, ForAll(set1, ForAll(elem1, Implies(add(t, set1, elem1), Not(contains(t, set1, elem1))))))) # can't add existing items

        # specify 'delete'
        # When deleting an object from set, it will not be contained in this set in the next point in time
        S.add(ForAll(self.t, ForAll(self.set1, ForAll(self.elem1, Implies(self.delete(self.t, self.set1, self.elem1),
                                                                          Not(self.contains(self.t + 1, self.set1,
                                                                                            self.elem1)))))))
        # Object is not contained in a set after deletion
        S.add(ForAll(self.t,
                     ForAll(self.set1, ForAll(self.elem1, Implies(Not(self.contains(self.t + 1, self.set1, self.elem1)),
                                                                  Not(self.delete(self.t + 1, self.set1,
                                                                                  self.elem1)))))))
        # Can't delete non-existent items
        S.add(ForAll(self.t, ForAll(self.set1, ForAll(self.elem1, Implies(self.delete(self.t, self.set1, self.elem1),
                                                                          self.contains(self.t, self.set1,
                                                                                        self.elem1))))))

        # S.add(ForAll(t, ForAll(set1, ForAll(elem1, Implies(delete(t, set1, elem1), Exists(t2, And(t2 > 0, t2 < t, add(t2, set1, elem1)))))))) # can't delete before add

        # Can't 'contain' without a previous 'add' which was not 'delete'
        S.add(ForAll(self.t, ForAll(self.set1, ForAll(self.elem1, Implies(self.contains(self.t, self.set1, self.elem1),
                                                                          Exists(self.t2,
                                                                                 And(self.t2 >= 0, self.t2 < self.t,
                                                                                     self.add(self.t2, self.set1,
                                                                                              self.elem1),
                                                                                     ForAll(self.t3, Implies(
                                                                                         And(self.t2 < self.t3,
                                                                                             self.t3 < self.t),
                                                                                         Not(
                                                                                             self.delete(
                                                                                                 self.t3,
                                                                                                 self.set1,
                                                                                                 self.elem1)))))))))))
