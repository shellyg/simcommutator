class PrintColors:
    def __init__(self):
        pass

    HEADER = '\033[1;30;50m'
    TITLE = '\033[1;30;50m'
    RED = '\033[1;31;50m'
    BLUE = '\033[1;34;50m'
    GREEN = '\033[1;32;50m'
    YELLOW = '\033[1;33;50m'
