from model import *


class Scenario(object):
    model = None

    def __init__(self, model):
        self.model = model

    def add_start_conditions(self, S):
        raise NotImplementedError("Must implement a scenario sub-class")

    def add_special_conditions(self, S):
        raise NotImplementedError("Must implement a scenario sub-class")

    def get_name(self):
        # Default implementation: return the class name. Optionally, this can be used for more meaningful names
        return self.__class__.__name__
