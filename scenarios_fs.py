from command_chain import *
from model_fs import *
from scenario import *


class ScenarioTestNLinks1(Scenario):
    def __init__(self, model):
        super(ScenarioTestNLinks1, self).__init__(model)

        if not isinstance(model, ModelSimpleFS):
            raise TypeError("This scenario requires a ModelSimpleFS")

        self.d1 = Const('d1', self.model.Directory)
        self.d2 = Const('d2', self.model.Directory)

        self.op_a = CommandChain(
            [[Command(self.model.inc_nlinks, 1, [self.d1]), Command(self.model.nop_nlinks, 1, [self.d2])],
             [Command(self.model.dec_nlinks, 1, [self.d2]), Command(self.model.nop_nlinks, 1, [self.d1])]])
        self.op_b = CommandChain(
            [[Command(self.model.inc_nlinks, 1, [self.d2]), Command(self.model.nop_nlinks, 1, [self.d1])],
             [Command(self.model.dec_nlinks, 1, [self.d1]), Command(self.model.nop_nlinks, 1, [self.d2])]])

    def add_start_conditions(self, S):
        S.add(self.model.get_nlinks(0, self.d1) == Int(0))
        S.add(self.model.get_nlinks(0, self.d2) == Int(0))

    def add_special_conditions(self, S):
        # constraints for distinct consts
        S.add(self.d1 != self.d2)


class ScenarioTestNLinks2(Scenario):
    def __init__(self, model):
        super(ScenarioTestNLinks2, self).__init__(model)

        if not isinstance(model, ModelSimpleFS):
            raise TypeError("This scenario requires a ModelSimpleFS")

        self.d1 = Const('d1', self.model.Directory)
        self.d2 = Const('d2', self.model.Directory)

        self.op_a = CommandChain(
            [[Command(self.model.inc_nlinks, 1, [self.d1]), Command(self.model.nop_nlinks, 1, [self.d2])],
             [Command(self.model.dec_nlinks, 1, [self.d1]), Command(self.model.nop_nlinks, 1, [self.d2])]])
        self.op_b = CommandChain(
            [[Command(self.model.inc_nlinks, 1, [self.d2]), Command(self.model.nop_nlinks, 1, [self.d1])],
             [Command(self.model.dec_nlinks, 1, [self.d2]), Command(self.model.nop_nlinks, 1, [self.d1])]])

    def add_start_conditions(self, S):
        S.add(self.model.get_nlinks(0, self.d1) == Int(0))
        S.add(self.model.get_nlinks(0, self.d2) == Int(0))

    def add_special_conditions(self, S):
        # constraints for distinct consts
        S.add(self.d1 != self.d2)


class ScenarioRenameDistinctFilesInDifferentDirectories(Scenario):
    def __init__(self, model):
        super(ScenarioRenameDistinctFilesInDifferentDirectories, self).__init__(model)

        if not isinstance(model, ModelSimpleFS):
            raise TypeError("This scenario requires a ModelSimpleFS")

        self.d1 = Const('d1', self.model.Directory)
        self.f1 = Const('f1', self.model.File)

        self.d2 = Const('d2', self.model.Directory)
        self.f2 = Const('f2', self.model.File)
        self.f3 = Const('f3', self.model.File)
        self.f4 = Const('f4', self.model.File)

        self.op_a = CommandChain([
            [
                Command(self.model.create_file, 2, [self.d1, self.f1]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.delete_file, 2, [self.d1, self.f1]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.dec_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.create_file, 2, [self.d1, self.f2]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ]
        ])
        self.op_b = CommandChain([
            [
                Command(self.model.create_file, 2, [self.d2, self.f3]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.delete_file, 2, [self.d2, self.f3]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.dec_nlinks, 1, [self.d2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.create_file, 2, [self.d2, self.f4]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ]
        ])

    def add_start_conditions(self, S):
        S.add(Not(self.model.contains(0, self.d1, self.f1)))
        S.add(Not(self.model.contains(0, self.d1, self.f2)))
        S.add(Not(self.model.contains(0, self.d1, self.f3)))
        S.add(Not(self.model.contains(0, self.d1, self.f4)))
        S.add(Not(self.model.contains(0, self.d2, self.f1)))
        S.add(Not(self.model.contains(0, self.d2, self.f2)))
        S.add(Not(self.model.contains(0, self.d2, self.f3)))
        S.add(Not(self.model.contains(0, self.d2, self.f4)))
        S.add(self.model.get_nlinks(0, self.d1) == 0)
        S.add(self.model.get_nlinks(0, self.d2) == 0)

    def add_special_conditions(self, S):
        # constraints for distinct consts
        S.add(self.d1 != self.d2)
        S.add(self.f1 != self.f2)
        S.add(self.f1 != self.f3)
        S.add(self.f1 != self.f4)
        S.add(self.f2 != self.f3)
        S.add(self.f2 != self.f4)


class ScenarioRenameDistinctFilesInDifferentDirectories2(Scenario):
    def __init__(self, model):
        super(ScenarioRenameDistinctFilesInDifferentDirectories2, self).__init__(model)

        if not isinstance(model, ModelSimpleFS):
            raise TypeError("This scenario requires a ModelSimpleFS")

        self.d1 = Const('d1', self.model.Directory)
        self.f1 = Const('f1', self.model.File)

        self.d2 = Const('d2', self.model.Directory)
        self.f2 = Const('f2', self.model.File)
        self.f3 = Const('f3', self.model.File)
        self.f4 = Const('f4', self.model.File)

        self.op_a = CommandChain([
            [
                Command(self.model.delete_file, 2, [self.d1, self.f1]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.dec_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.create_file, 2, [self.d1, self.f2]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ]
        ])
        self.op_b = CommandChain([
            [
                Command(self.model.delete_file, 2, [self.d2, self.f3]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.dec_nlinks, 1, [self.d2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.create_file, 2, [self.d2, self.f4]),
                Command(self.model.nop_nlinks, 1, [self.d1]),
                Command(self.model.nop_nlinks, 1, [self.d2])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ]
        ])

    def add_start_conditions(self, S):
        #S.add(self.model.create_file(0, self.d1, self.f1))
        #S.add(self.model.create_file(0, self.d2, self.f3))
        S.add(self.model.contains(0, self.d1, self.f1))
        S.add(Not(self.model.contains(0, self.d1, self.f2)))
        S.add(Not(self.model.contains(0, self.d1, self.f3)))
        S.add(Not(self.model.contains(0, self.d1, self.f4)))
        S.add(Not(self.model.contains(0, self.d2, self.f1)))
        S.add(Not(self.model.contains(0, self.d2, self.f2)))
        S.add(self.model.contains(0, self.d2, self.f3))
        S.add(Not(self.model.contains(0, self.d2, self.f4)))
        S.add(self.model.get_nlinks(0, self.d1) == 1)
        S.add(self.model.get_nlinks(0, self.d2) == 1)

    def add_special_conditions(self, S):
        # constraints for distinct consts
        S.add(self.d1 != self.d2)
        S.add(self.f1 != self.f2)
        S.add(self.f1 != self.f3)
        S.add(self.f1 != self.f4)
        S.add(self.f2 != self.f3)
        S.add(self.f2 != self.f4)


class ScenarioAddTwoDistinctFilesToTheSameDirectory(Scenario):
    def __init__(self, model):
        super(ScenarioAddTwoDistinctFilesToTheSameDirectory, self).__init__(model)

        if not isinstance(model, ModelSimpleFS):
            raise TypeError("This scenario requires a ModelSimpleFS")

        self.d1 = Const('d1', self.model.Directory)
        self.f1 = Const('f1', self.model.File)
        self.f2 = Const('f2', self.model.File)

        self.op_a = CommandChain([
            [
                Command(self.model.create_file, 2, [self.d1, self.f1]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1])
            ]
        ])
        self.op_b = CommandChain([
            [
                Command(self.model.create_file, 2, [self.d1, self.f2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1])
            ]
        ])

    def add_start_conditions(self, S):
        S.add(Not(self.model.contains(0, self.d1, self.f1)))
        S.add(Not(self.model.contains(0, self.d1, self.f2)))
        S.add(self.model.get_nlinks(0, self.d1) == 1)

    def add_special_conditions(self, S):
        # constraints for distinct consts
        S.add(self.f1 != self.f2)


class ScenarioAddTheSameFileToTheSameDirectory(Scenario):
    def __init__(self, model):
        super(ScenarioAddTheSameFileToTheSameDirectory, self).__init__(model)

        if not isinstance(model, ModelSimpleFS):
            raise TypeError("This scenario requires a ModelSimpleFS")

        self.d1 = Const('d1', self.model.Directory)
        self.f1 = Const('f1', self.model.File)
        self.f2 = Const('f2', self.model.File)

        self.op_a = CommandChain([
            [
                Command(self.model.create_file, 2, [self.d1, self.f1]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1])
            ]
        ])
        self.op_b = CommandChain([
            [
                Command(self.model.create_file, 2, [self.d1, self.f2]),
                Command(self.model.nop_nlinks, 1, [self.d1])
            ],
            [
                Command(self.model.inc_nlinks, 1, [self.d1])
            ]
        ])

    def add_start_conditions(self, S):
        S.add(Not(self.model.contains(0, self.d1, self.f1)))
        S.add(Not(self.model.contains(0, self.d1, self.f2)))
        S.add(self.model.get_nlinks(0, self.d1) == 1)

    def add_special_conditions(self, S):
        # constraints for distinct consts
        S.add(self.f1 == self.f2)
