from command_chain import *
from model_sets import *
from scenario import *


class ScenarioA(Scenario):

    def __init__(self, model):
        super(ScenarioA, self).__init__(model)

        if not isinstance(model, ModelSimpleSets):
            raise TypeError("This scenario requires a ModelSimpleSets")

        self.s1 = Const('s1', self.model.Set)
        self.e1 = Const('e1', self.model.Elem)

        self.s2 = Const('s2', self.model.Set)
        self.e2 = Const('e2', self.model.Elem)

        self.op_a = CommandChain([Command(self.model.add, 2, [self.s1, self.e1]),
                                  Command(self.model.delete, 2, [self.s2, self.e2])])
        self.op_b = self.op_a

    def add_start_conditions(self, S):
        S.add(Not(self.model.contains(0, self.s1, self.e1)))
        S.add(Not(self.model.contains(0, self.s2, self.e2)))

    def add_special_conditions(self, S):
        # if want distinct consts s1,s2, e1,e2:
        S.add(self.s1 != self.s2)
        S.add(self.e1 != self.e2)


class ScenarioB(ScenarioA):

    def __init__(self, model):
        super(ScenarioB, self).__init__(model)

    def add_special_conditions(self, S):
        # if want equal consts
        S.add(self.s1 == self.s2)
        S.add(self.e1 == self.e2)
